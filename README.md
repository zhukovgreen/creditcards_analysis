# Creditcards data analysis

## Tasks within the assignment (Acceptance criteria):

- [X] Use the dataset (`creditcard.csv`)
- [X] Implement your own Logistic Regression (https://en.wikipedia.org/wiki/Logistic_regression) estimator from scratch using arbitrary estimation method:
	- [X] Avoid using 3rd party libraries like Scikit-learn and similar, which already contain this type of Model
	- [X] Avoid using 3rd party libraries which implement e.g. gradient descent, for optimizing the model
	- [X]  You can use Numpy and similar, to operate on matrices.
- [X]  Use the logistic regression model you built to predict the probability of default. The variables name is `DEFAULT_PAYMENT_NEXT_MONTH`. There is a dictionary that explains all the features included in the dataset called `dictionary.txt`.
- [X]  Produce a report that summarizes your findings. Things to include in the report:
	- [X]  Describe the features and the target included in the dataset. What insight did you gain from working with the data?
	- [X]  Explain how you transformed the features, if at all
	- [X]  The validation approach taken
	- [X]  Which performance metrics did you use and why?
	- [X]  What were the most important features?
	- [X]  How useful is the model from a practical point of view? 
	- [X]  What might you do differently if you had more time/resources?
- [X]  This is not meant to be an exercise to develop a ‘final solution’ but more the first pass, to understand how you would approach this kind of exercise.
- [X]  Submit the report and any source code (these may be part of the same document if you are working with a Jupyter notebook, etc.).


## Report

The workbook, which has some useful visualization can be found here:
![solution notebook](notebooks/Preliminary data exploration.ipynb)
It is recommended to walk through it before continue reading this report.

My implementation of the logistic regression is at `In [19]` cell. 

### Describe the features and the target included in the dataset. What insight did you gain from working with the data?

Most descriptive information can be seen in `out [3]` cell, where we see the histograms of all features.
Some of the features were skewed and required a transformation (i.e. gender or limit balance). Most of the data
required to be normalized and I applied one hot encoding to features like gender, education, PAY_{i}

### Explain how you transformed the features, if at all

I normalized all the features, so they ranged from 0 to 1. For skewed distributions, log normalization was
applied. Then, categorical features were one hot encoded.

If to compare the results of a logistic classifier for transformed and not transformed features then you see
the following:

**Data transformation applied**

```
              precision    recall  f1-score   support

           0       0.83      0.96      0.89      4632
           1       0.69      0.33      0.45      1368

    accuracy                           0.81      6000
   macro avg       0.76      0.64      0.67      6000
weighted avg       0.80      0.81      0.79      6000
```

**No data transformation applied**

```
              precision    recall  f1-score   support

           0       0.77      1.00      0.87      4632
           1       0.00      0.00      0.00      1368

    accuracy                           0.77      6000
   macro avg       0.39      0.50      0.44      6000
weighted avg       0.60      0.77      0.67      6000
```

### The validation approach taken
In `Out[14]` cell I did a plot, which shows the precision-recall value as a function of the probability threshold.
This gives me insights into how to deal with the imbalanced target labels. By reducing the threshold to 0.3..0.4,
the recall improved:

**Improved threshold**
```
              precision    recall  f1-score   support

           0       0.87      0.84      0.85      4632
           1       0.51      0.56      0.54      1368

    accuracy                           0.78      6000
   macro avg       0.69      0.70      0.69      6000
weighted avg       0.79      0.78      0.78      6000
```

I also tried to do a grid search with F2 score to do a hyperopt, but this didn't give me significant results.

### Which performance metrics did you use and why?

I was using the confusion matrix and checked the good balance in precision and recall. 
Due to a little number of labels with value 1, it is hard to get a good classifier.

### What were the most important features?

From `In [17]` it is definitely PAY_2: Repayment status in August, 2005

### How useful is the model from a practical point of view?

It can be safely used for predicting default.payment.next.month is 0=No. 1 - has a bad recall.
This means, the quality of the classifier in terms of predicting the default payment next month is `yes`
will be very conservative.

### What might you do differently if you had more time/resources?

I would definitely improve my implementation of the logistic regression, as it is really
minimalistic now (see `In [19]`).

Then I would start to do a more deep analysis of how to improve the recall ratio. Now I see only two options:
1. Ask friends
2. Add more data to the dataset

## Installation
You have to have `poetry` installed on your host. Then:

```bash
poetry install
poetry run jupyter notebook
```

